  #!/usr/bin/env python
# _*_ coding:utf-8 _*_

import requests
import os
import time
from bs4 import BeautifulSoup

headers = {
        # 'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        # 'Accept-Encoding':'gzip, deflate, br',
        # 'Accept-Language':'zh-CN,zh;q=0.9',
        # 'Cache-Control':'max-age=0',
        # 'Connection':'keep-alive',
        # 'Upgrade-Insecure-Requests':'1',
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
        #'Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3650.400 QQBrowser/10.4.3341.400'
    }
Num = 100
timesleep = 2
ipPool = []

def main():

    url = "https://www.kuaidaili.com"
    if not get_index (url):
        return None
    print('开始下载IP')
    print ('下载数量%s，间隔%s秒' %(Num,timesleep))
    time.sleep(2)
    download()

def get_index(url):
    """
    访问首页
    """
    response = get_status(url)
    if response:
        print("首页,建立连接...")
        return True
    else:
        print("ERROR: 首页访问失败！")
        return False

def download():
    """
    下载
    """
    ipList = getIpList()
    path = os.path.join (os.getcwd (), "IPPOOL.txt")   #返回当前工作目录

    for ip in ipList:
        write_to_text(path,ip)

    print('已下载%s个ip地址' %len(ipList))

def get_last_page(url):
    """
    获取最后一页page
    """
    response = get_status(url)
    if not response:
        return None
    html = response.text
    soup = BeautifulSoup(html, "html5lib")
    lis = soup.select("#listnav > ul > li")

    if lis[-1].text == "页":
        last_page = lis[-2].find("a").text

        return int(last_page)
    return None

def get_status(url):
    """
    获取状态
    """
    response = requests.post(
        url=url,
        headers=headers,
    )
    if response.status_code == 200:
        return response
    else:
        print("ERROR: 网络连接失败！ status: %s url: %s" % (response.status_code, url))
        return False


def getIpList():
    """
    获取ip地址
    """
    url = 'https://www.kuaidaili.com/free/inha/1/'
    last_page = get_last_page (url)

    ipList = []

    for i in range (1, last_page + 1):
        time.sleep (2)
        url = "https://www.kuaidaili.com/free/inha/{i}".format (i=i)


        response = get_status(url)
        if not response:
            return None
        html = response.text
        soup = BeautifulSoup(html, 'html.parser')
        items = soup.find(id="list").find("tbody").find_all("tr")

        for item in items:
            ip = item.find(attrs={"data-title": "IP"}).text
            port = item.find(attrs={"data-title": "PORT"}).text
            ip_port = ip + ":" + port + "\n"
            ipList.append(ip_port)

            if len(ipList) == Num:
                break
        if len (ipList) == Num:         #跳出多层循环
            break

    return ipList

def write_to_text(path,content):
    """
    写
    """
    path = os.path.abspath(path) #获得文件当前路径
    with open(path,'a+', encoding='utf-8') as f:
        f.write(content)

def check_ip(ip):
    """
    检查ip是否可用 略慢
    :return:
    """
    test_url = 'https://www.baidu.com'
    proxy = {'http': ip}
    try:
        response = requests.get(test_url, headers=headers, proxies=proxy, timeout=5)
        time.sleep(1)
        if response.status_code == 200:
            return True
        else:
            return False
    except Exception as e:
        print(e)
        return False
        time.sleep(1)


if __name__ == '__main__':
    main()
