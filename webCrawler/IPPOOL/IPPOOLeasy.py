#!/usr/bin/env python  
# _*_ coding:utf-8 _*_

import requests
import os
import time
from bs4 import BeautifulSoup

headers = {
        'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding':'gzip, deflate, br',
        'Accept-Language':'zh-CN,zh;q=0.9',
        'Cache-Control':'max-age=0',
        'Connection':'keep-alive',
        'Upgrade-Insecure-Requests':'1',
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
        'Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3650.400 QQBrowser/10.4.3341.400'
    }
page = 10
timesleep = 2

def main():
    print('开始下载IP')
    print ('下载前%s页，间隔%s秒' %(page,timesleep))
    download()

def get_status(url):
    """
    获取状态
    :param url: 访问地址
    :return: 返回response或False
    """
    response = requests.post(
        url=url,
        headers=headers
    )
    if response.status_code == 200:
        return response
    else:
        print("ERROR: 网络连接失败！ status: %s url: %s" % (response.status_code, url))
        return False

def download():
    for i in range(1,page+1):
        time.sleep(timesleep)
        url = "https://www.kuaidaili.com/free/inha/{i}".format (i=i)
        print (url)
        ipList = getIpList(url)
        path = os.path.join(os.getcwd(), "IPPOOL.txt")
        write_to_text(path,ipList)

def getIpList(url):
    response = get_status(url)
    if not response:
        return None
    html = response.text
    soup = BeautifulSoup(html, 'html.parser')
    items = soup.find(id="list").find("tbody").find_all("tr")
    ipList = []
    for item in items:
        ip = item.find(attrs={"data-title": "IP"}).text
        port = item.find(attrs={"data-title": "PORT"}).text
        ip = ip + ":" + port + "\n"
        ipList.append(ip)

    return ipList

def write_to_text(path,content):
    path = os.path.abspath(path)
    with open(path,'a+', encoding='utf-8') as f:
        f.writelines(content)
    f.close()


if __name__ == '__main__':
    main()
