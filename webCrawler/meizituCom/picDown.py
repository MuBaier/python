'''
爬取妹子网
忽略每日更新(带更新)
建议 写入延时
'''

import requests
import json
from lxml import etree
# etree报错 不影响使用
import os
import mkdirs

class downLoad(object):
    def __init__(self):
        self.headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36",
            "Referer": "https://www.mzitu.com/"
        }

    def downPic(self):
        mkdirs.mkdir("download")
        url = self.change()
        mkdirs.mkdir("download/"+self.title)
        #url = 'https://www.mzitu.com/'
        page = self.getPage(url)
        pageDown = int(input('共有%s页，下载前几页：' %page))
        #pageDown = 2

        for p in range(1,pageDown+1):
            u = url + 'page/' + str(p)+'/'
            urls = self.getUrl(u)





    def getUrl(self,url):
        response = requests.get(url,headers=self.headers)
        #print(url)
        #print(response.text)
        html = etree.HTML(response.content.decode())
        urlList = html.xpath('//ul[@id="pins"]/li/span/a/@href')
        nameList = html.xpath('//ul[@id="pins"]/li/span/a/text()')
        # print(urlList,len(nameList))

        for i in range(len(urlList)):
            path = '/download/' +self.title+'/'+ nameList[i]
            mkdirs.mkdir(path)
            page = int(self.getPage2(urlList[i]))
            print(urlList[i])
            self.down(urlList[i],page,path)


    #下载
    def down(self,url,page,path):
        for p in range(1,page+1):
            u = url + '/' + str(p)
            #print(u)
            response = requests.get(u, headers=self.headers)
            html = etree.HTML(response.content.decode())
            #print(html)
            src = html.xpath('//div[@class="main-image"]/p/a/img/@src')[0]
            print(src)

            pic = requests.get(src,headers = self.headers)

            with open(os.getcwd()+path+'/{}.jpg'.format(p),'wb') as f:
                f.write(pic.content)

    #选择下载内容
    def change(self):
        url = 'https://www.mzitu.com'
        response = requests.get(url,headers = self.headers)
        #print(response.text)
        #注意 这里返回的response.text 不是json所期待的参数
        #data = json.loads(response.text)
        html = etree.HTML(response.content.decode())
        title = html.xpath('//ul[@id="menu-nav"]/li/a/@title')
        hrefs = html.xpath('//ul[@id="menu-nav"]/li/a/@href')
        #print(titles,hrefs)
        for i in range(len(title)):
            print(i,title[i])
        h = input('请选择下载内容：')
        while (int(h) < 0) or (int(h) > 7):
            h = input('请选择下载内容：')
        h = int(h)
        self.title = title[h]
        return hrefs[h]

    #获取最大页书
    def getPage(self,url):
        response = requests.get(url, headers=self.headers)
        #print(response.text)
        html = etree.HTML(response.content.decode())
        page = html.xpath('//div[@class="nav-links"]/a/text()')[-2]
        return page

    # 获取最大页书
    def getPage2(self, url):
        response = requests.get(url, headers=self.headers)
        #print(response.text)
        html = etree.HTML(response.content.decode())
        page = html.xpath('//div[@class="pagenavi"]/a/span/text()')[-2]
        #print(page)
        return page

downLoad = downLoad()
downLoad.downPic()