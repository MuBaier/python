import requests
import jsonpath
import json #内置
import mkdirs  #learning/OS/mkdirs
import urllib

url = "https://www.duitang.com/napi/blog/list/by_search/?kw={}&start={}"
label = input("下载内容：")
#编码
label = urllib.parse.quote(label)
#print(label)

num = 0

headers = {
# 一个特殊字符串头，使得服务器能够识别客户使用的操作系统及版本、CPU 类型、浏览器及版本、浏览器渲染引擎、浏览器语言、浏览器插件等。
'UserAgent' : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36 QQBrowser/4.5.122.400"

}

#创建文件夹
mkdirs.mkdir("pic")

for index in range(0,1000,24):

    if index%48 == 0 and index != 0:
        print("已下载%s张" %num)
        on = input("是否继续(Y or N)")
        if on.upper() == "N":
            break

    u = url.format(label,index)

    response = requests.get(u,headers=headers)

    data = response.text  #字符串
    #转换为json格式   字典格式
    html = json.loads(data)
    #print(html['data']['object_list']...)

    #列表 (所有的path值)
    photo = jsonpath.jsonpath(html,"$..path")
    num = num + len(photo)

    for i in photo:
        pic = requests.get(i)
        p = i.split("/")[-1]
        p = p.split(".")[0]
        #print(p)

        #写
        #with open 提供了自动关闭的功能
        with open('pic/{}.jpg'.format(p),'wb') as f:
            f.write(pic.content)

