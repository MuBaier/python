'''
微信自动回复  **伪人工智障**
基于图灵机器人api
'''

import itchat
import requests

itchat.auto_login(hotReload=True)

#获取微信好友的发的信息

apiUrl = 'http://www.tuling123.com/openapi/api'

def get_info(message):
    data = {
        'key' : "57da7b2e24be4dc1b75fbce444d4711c",
        'info' : message,
        'userid' :'robot'
    }
    try:
        r = requests.post(apiUrl,data=data).json()
        info = r['text']
        print('me:%s' %info)
        return info
    except:
        return

#回复
@itchat.msg_register(itchat.content.TEXT)
def auto_reply(msg):
    defaultReply = "我知道了"
    #搜索微信好友
    realFriend = itchat.search_friends(name='Kidi')
    #print(realFriend)
    realFriendName = realFriend[0]['UserName']
    #调用接口
    print('others:%s' % msg['Text'])
    reply = get_info(msg['Text'])
    if msg['FromUserName'] == realFriendName:
        #发送消息
        itchat.send(reply,toUserName=realFriendName)

itchat.run()
#会自动运行装饰器下的代码