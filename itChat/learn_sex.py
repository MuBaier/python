'''
统计好友性别
sex 1 男
    2 女
    0 未设置性别
'''

import itchat

#登陆
#itchat.login() #单次登陆
itchat.auto_login(hotReload=True)  #自动登陆  有时效 大概十分钟
#记录了登陆信息 见itchat.pkl

#获取微信好友列表 json格式
friends = itchat.get_friends()

man = 0
woman = 0
other = 0

for i in friends:
    sex = i['Sex']
    #print(i)

    if sex == 1:
        man += 1
    elif sex == 2:
        woman += 1
    else:
        other += 1

#微信好友总数 算上自己 应该是第一个  不要可以[1:]去掉
total = len(friends)

print('好友总数：%s' %total)
print('男性占比:%.2f%%' %(float(man)/total*100))
