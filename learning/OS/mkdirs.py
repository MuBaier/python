import os

'''
创建文件夹
name： 文件夹名
path：地址（默认当前文件夹）
'''

#os.getcwd() 查看当前路径
def mkdir(name,path = os.getcwd()):
    path = path+'/'+name
    if not os.path.exists(path):
        print("已创建%s"  %path)
        os.mkdir(path)
    else:
        print("%s存在" %path)