#!/usr/bin/env python  
# _*_ coding:utf-8 _*_

"""
验证码制作
pip库：pillow
"""


from PIL import Image,ImageFilter,ImageFont,ImageDraw
import random
import string

#随机图片
def randChar():
    return chr(random.randint(65,90))

#随机字母和数字
def getrand1(num,many):
    for x in range(many):
        s = ''
        for i in range(num):
            n = random.randint(1,2) #1为数字 2为字母
            if n == 1:
                numb = random.randint(0,9)
                s += str(numb)
            else:
                s += str(random.choice(string.ascii_letters))
    return s

# 随机颜色1:
def rndColor():
    return (random.randint(64, 255), random.randint(64, 255), random.randint(64, 255))

# 随机颜色2:
def rndColor2():
    return (random.randint(32, 127), random.randint(32, 127), random.randint(32, 127))

width = 60 * 4
height = 60

image = Image.new('RGB',(width,height),(255,255,255))    #创建画布
font = ImageFont.truetype('C:\Windows\Fonts\Arial.ttf',36)   #指定字体 大小
draw = ImageDraw.Draw(image)

# 填充每个像素:
for x in range(width):
    for y in range(height):
        draw.point((x, y), fill=rndColor())

for x in range(4):
    draw.text((60*x+10,10),getrand1(1,4),font=font,fill=rndColor2())

#模糊处理
#image = image.filter(ImageFilter.BLUR)

image.show()